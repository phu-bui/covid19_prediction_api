from __future__ import division, print_function
from flask import Flask, request, jsonify
import utils
import numpy as np
from tensorflow.keras.models import load_model
from tensorflow.keras.preprocessing import image
from tensorflow.keras.preprocessing.image import img_to_array
import os
import tensorflow as tf
from flask import Flask, request

app = Flask(__name__)
# Model saved with Keras model.save()
MODEL_PATH = './model/my_model'

# Load your trained model
model = load_model(MODEL_PATH)

@app.route('/')
def hello_world():
    return "Hello World!"

def model_predict(img_path, model):
    img = image.load_img(img_path, target_size=(150, 150))
    x = img_to_array(img) / 255
    x = np.expand_dims(x, axis=0)
    images = np.vstack([x])
    predict = model.predict(images, batch_size=32)
    return predict

@app.route('/predict', methods=['POST', 'GET'])
def classify():
    try:
        if request.method == 'GET':
            # check if the post request has the image part
            if 'image' not in request.files:
                return jsonify({
                    'message': 'No file part'
                }), 400
            file = request.files['image']
            # if user does not select file, browser also
            # submit an empty part without filename
            if file.filename == '':
                return jsonify({
                    'message': 'No selected file'
                }), 400
            filename = utils.save_upload_file(file)
            return jsonify({
                'method': "POST",
                'filename': filename
            })
        elif request.method == 'POST' and request.args.get('image_url', '') != '':
            image_url = request.args.get('image_url')
            filename = utils.download_image_from_url(image_url)
            basepath = os.path.dirname(__file__)
            file_path = os.path.join(
                basepath, 'uploaded', filename)
            predict = model_predict(file_path, model)

            # Arrange the correct return according to the model.
            # In this model 1 is Pneumonia and 0 is Normal.
            str0 = 'Covid19'
            str1 = 'Normal'
            str2 = 'Pneumonia'
            print(predict[0])
            if predict[0][0] > predict[0][1] and predict[0][0] > predict[0][2]:
                acc = predict[0][0]*100
                return jsonify({
                    'image_url': image_url,
                    'file_name': filename,
                    'result': str0,
                    'accuracy': acc
                })
            elif predict[0][1] > predict[0][0] and predict[0][1] > predict[0][2]:
                acc = predict[0][1] * 100
                return jsonify({
                    'image_url': image_url,
                    'file_name': filename,
                    'result': str1,
                    'accuracy': acc
                })
            else:
                acc = predict[0][2] * 100
                return jsonify({
                    'image_url': image_url,
                    'file_name': filename,
                    'result': str2,
                    'accuracy': acc
                })
        else:
            return jsonify({
                'message': 'Action is not defined!'
            }), 404
    except Exception as e:
        return repr(e), 500


if __name__ == '__main__':
    app.run(debug=True, host='127.0.0.1', port=5000)